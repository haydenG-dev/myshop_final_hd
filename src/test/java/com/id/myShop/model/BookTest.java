package com.id.myShop.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BookTest {
	protected Book book;
	
	@Before
	public void setUp() throws Exception {
		book = new Book();
	}
	
	
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testName () {
		book.setName("test");
		assertEquals("test",book.getName());
	}
	@Test
	public void testId () {
		book.setId(1);
		assertEquals(1,book.getId());
	}
	
	@Test
	public void testNbrElet () {
		book.setNbrElet(1);
		assertEquals(1,book.getNbrElet());
	}
}
