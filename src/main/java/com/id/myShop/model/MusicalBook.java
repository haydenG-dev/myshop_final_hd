package com.id.myShop.model;

import java.util.List;

public class MusicalBook extends Book {
	
	List<String>listOfSound;
	int lifeTime;
	int nbrBattery;
	public List<String> getListOfSound() {
		return listOfSound;
	}
	public void setListOfSound(List<String> listOfSound) {
		this.listOfSound = listOfSound;
	}
	public int getLifeTime() {
		return lifeTime;
	}
	public void setLifeTime(int lifeTime) {
		this.lifeTime = lifeTime;
	}
	public int getNbrBattery() {
		return nbrBattery;
	}
	public void setNbrBattery(int nbrBattery) {
		this.nbrBattery = nbrBattery;
	}
	
	
	

}
