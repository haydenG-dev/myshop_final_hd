package com.id.myShop.model;

public class Paper extends Consumable{
	
	String quality;
	Float Weight;
	
	
	public String getQuality() {
		return quality;
	}
	public void setQuality(String quality) {
		this.quality = quality;
	}
	public Float getWeight() {
		return Weight;
	}
	public void setWeight(Float weight) {
		Weight = weight;
	}

}
